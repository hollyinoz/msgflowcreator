# MsgflowCreator

A simple nodejs application to create a skeleton message flow. 

The intent is to remove the typical boiler plate code that is required for each business service as per Bendigo Banks 
Message Broker framework.