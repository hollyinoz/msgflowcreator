const fs = require('fs');
const path = require('path');

module.exports = class IOController {

	constructor() {
		this.directoryPath = null;
		this.data = "Hello world, this is data";
	}

	async readFile(filePath) {

	}

	writeFile(filePath, fileName) {
		if(filePath && fileName){
			let path = filePath + fileName;
			let writer = fs.createWriteStream(path);
			writer.write(this.data, (err) => {
				if(err) {
					console.log(err.message);
				}
				else {
					console.log(`file written to ${path}`);
				}
			});
		}
		else {
			console.log("file path or file name were missing");
		}
	}

	createDirectory(directoryName) {

	}

	createPackage(packageName) {

	}

}