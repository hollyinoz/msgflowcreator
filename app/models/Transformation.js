
module.exports  = class TransformationTemplate {

	constructor(req) {
		this.projectDirectory = req.body.projectDirectory;
		this.packageName = req.body.packageName;
		this.classDescription = req.body.classDescription;
		this.serviceId = req.body.serviceId;
		this.requestMessageName = req.body.reqMessageName;
		this.authenticationName = req.body.authenticationName;
		this.endPoint = req.body.endPoint;
		//this.validationFields
		this.businessRequestNameSpace = req.body.businessRequestNameSpace;
		this.businessRequestMsgName = req.body.businessRequestMsgName;
		this.requestMessageFields = req.body.requestMessageFields;
		this.responseMessageName = req.body.responseMessageName;
		this.businessResponseMsgName = req.body.businessResponseMsgName;
		this.responseNameSpace = req.body.responseNameSpace;
		this.responseMessageFields = req.body.responseMessageFields;

		this.requiresCredentialsNode = req.body.requiresCredentialsNode;
		this.requiresHTTPNode = req.body.requiresHTTPNode;
		//this.errorResponseName = req.data;
		//this.responseMessage = req.data;
		//this.responseMessageData = req.data;

	}
}