
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const env = process.env.REGION || 'dev'; //TODO: Change to tiny for prod
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
let transformationValidator = require('./validators/TransformationFormValidator');
let ioController = require('./controllers/IOController');
let transformationTmpl = require('./models/Transformation');


app.use(logger(env));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));

//ioController = new ioController();
//ioController.writeFile('./app/', 'test.txt');

app.get('/', (req, res) => {
	// res.setHeader("Content-Type", "text/html");
	res.sendFile('index.html');
});

app.post('/create/msgflow', (req, res) => {
	console.log("form submitted ");
	const ok = 'OK';
	let transformation = new transformationTmpl(req);
	let transformValidator = new transformationValidator(transformation);
	let status = transformValidator.isValidTransformation() ? ok : transformValidator.fieldError;
	if(status === ok) {

	}
	res.send(status);
});

app.listen(port, () => console.log(`Server listening on port ${port}!`));