
const stringUtils = require('../utils/StringUtils');

module.exports = class TransformationFormValidator {

	constructor(transformation) {
		this.data = transformation;
		this.stringValidator = new stringUtils();
		this.fieldError = '';
	}

	validateForm() {
		this.isError(this.stringValidator.isValidString(this.data.projectDirectory), 'Project Directory is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.packageName), 'Package Name is a required field.');
		this.isError(this.stringValidator.isNumber(this.data.serviceId), 'ServicedID ' + this.data.serviceId + ' is not a number.'); //Number
		this.isError(this.stringValidator.isValidString(this.data.requestMessageName), 'Request Message Name is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.authenticationName), 'Authentication Name is a required field.');
		this.isError(this.isValidEndPoint(this.data.endPoint), this.fieldError);
		this.isError(this.stringValidator.isValidString(this.data.businessRequestNameSpace), 'Business Request Namespace Name is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.businessRequestMsgName), 'Business Request Message Name is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.responseMessageName), 'Response Message Name is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.businessResponseMsgName), 'Business Response Message Name is a required field.');
		this.isError(this.stringValidator.isValidString(this.data.responseNameSpace), 'Response Namespace is a required field.');
	}


	isValidEndPoint(value) {
		if(!this.stringValidator.isValidString(value)){
			this.fieldError = 'Endpoint is a required field.';
			return false;
		}
		if(!this.stringValidator.dotExists(value)){
			this.fieldError = 'Invalid Endpoint, does not contain a dot(.)';
			return false;
		}
		return true;
	}

	isError(isValid, msg) {
		if(!isValid) {
			//throw some kind of error with msg
		}
	}

}